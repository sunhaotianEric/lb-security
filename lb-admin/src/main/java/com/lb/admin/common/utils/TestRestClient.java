package com.lb.admin.common.utils;

import com.alibaba.fastjson.JSON;
import com.lb.admin.modules.manage.entity.BroadcastEntity;
import org.apache.http.HttpEntity;
import org.apache.http.ParseException;
import org.apache.http.client.ClientProtocolException;
import org.apache.http.client.methods.CloseableHttpResponse;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.entity.StringEntity;
import org.apache.http.impl.client.CloseableHttpClient;
import org.apache.http.impl.client.HttpClientBuilder;
import org.apache.http.util.EntityUtils;
import org.springframework.beans.factory.annotation.Value;

import java.io.IOException;
import java.util.HashMap;
import java.util.Map;

public class TestRestClient {
//    @Value("${rest.lb_back_end_url}")
    @Value("${rest.lb_chat_url}")
    private static String REST_URL;

    public static void main(String[] args) {
        BroadcastEntity broadcast = new BroadcastEntity();
        broadcast.setContent("大家好");
        broadcast.setId(2);
        broadcast.setGroupId(1);
        broadcast.setTime(6546555l);
        broadcast.setWay(0);
        Map<String ,Object> parms=new HashMap<>();
        parms.put("broadcast",broadcast);
        doPostTestTwo("api/update_broadcast", parms);
    }
    public static void doPostTestTwo(String url,Map<String ,Object> parms) {

        // 获得Http客户端(可以理解为:你得先有一个浏览器;注意:实际上HttpClient与浏览器是不一样的)
        CloseableHttpClient httpClient = HttpClientBuilder.create().build();

        // 创建Post请求
        System.err.println(REST_URL+url);
        HttpPost httpPost = new HttpPost(/*REST_URL*/"http://localhost:3001/"+url);


        // 我这里利用阿里的fastjson，将Object转换为json字符串;
        // (需要导入com.alibaba.fastjson.JSON包)
        String jsonString = JSON.toJSONString(parms);

        StringEntity entity = new StringEntity(jsonString, "UTF-8");

        // post请求是将参数放在请求体里面传过去的;这里将entity放入post请求体中
        httpPost.setEntity(entity);

        httpPost.setHeader("Content-Type", "application/json;charset=utf8");

        // 响应模型
        CloseableHttpResponse response = null;
        try {
            // 由客户端执行(发送)Post请求
            response = httpClient.execute(httpPost);
            // 从响应模型中获取响应实体
            HttpEntity responseEntity = response.getEntity();

            System.out.println("响应状态为:" + response.getStatusLine());
            if (responseEntity != null) {
                System.out.println("响应内容长度为:" + responseEntity.getContentLength());
                System.out.println("响应内容为:" + EntityUtils.toString(responseEntity));
            }
        } catch (ClientProtocolException e) {
            e.printStackTrace();
        } catch (ParseException e) {
            e.printStackTrace();
        } catch (IOException e) {
            e.printStackTrace();
        } finally {
            try {
                // 释放资源
                if (httpClient != null) {
                    httpClient.close();
                }
                if (response != null) {
                    response.close();
                }
            } catch (IOException e) {
                e.printStackTrace();
            }
        }
    }

}
