package com.lb.admin.modules.manage.dao;

import com.lb.admin.modules.manage.entity.UserEntity;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import org.apache.ibatis.annotations.Mapper;
import org.apache.ibatis.annotations.Param;

/**
 *
 *
 * @author Alex
 * @email lbjt201910@gmail.com
 * @date 2019-10-04 17:06:26
 */
@Mapper
public interface UserDao extends BaseMapper<UserEntity> {
    /**
     * 根据key，更新value
     */
    int updateGoldcoin(@Param("id") Long id, @Param("money") Double money);

}
