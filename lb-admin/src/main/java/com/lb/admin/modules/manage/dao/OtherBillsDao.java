package com.lb.admin.modules.manage.dao;

import com.lb.admin.modules.manage.entity.OtherBillsEntity;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import org.apache.ibatis.annotations.Mapper;

/**
 * 
 * 
 * @author Alex
 * @email lbjt201910@gmail.com
 * @date 2019-11-21 13:38:27
 */
@Mapper
public interface OtherBillsDao extends BaseMapper<OtherBillsEntity> {
	
}
