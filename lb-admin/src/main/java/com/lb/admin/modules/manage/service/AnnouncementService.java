package com.lb.admin.modules.manage.service;

import com.baomidou.mybatisplus.extension.service.IService;
import com.lb.admin.common.utils.PageUtils;
import com.lb.admin.modules.manage.entity.AnnouncementEntity;

import java.util.Map;

/**
 * 
 *
 * @author Alex
 * @email lbjt201910@gmail.com
 * @date 2019-10-14 14:32:35
 */
public interface AnnouncementService extends IService<AnnouncementEntity> {

    PageUtils queryPage(Map<String, Object> params);
}

