package com.lb.admin.modules.manage.service.impl;

import org.springframework.stereotype.Service;
import java.util.Map;
import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.baomidou.mybatisplus.core.metadata.IPage;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.lb.admin.common.utils.PageUtils;
import com.lb.admin.common.utils.Query;

import com.lb.admin.modules.manage.dao.MakeUpDao;
import com.lb.admin.modules.manage.entity.MakeUpEntity;
import com.lb.admin.modules.manage.service.MakeUpService;


@Service("makeUpService")
public class MakeUpServiceImpl extends ServiceImpl<MakeUpDao, MakeUpEntity> implements MakeUpService {

    @Override
    public PageUtils queryPage(Map<String, Object> params) {
        IPage<MakeUpEntity> page = this.page(
                new Query<MakeUpEntity>().getPage(params),
                new QueryWrapper<MakeUpEntity>()
        );

        return new PageUtils(page);
    }

}
