package com.lb.admin.modules.manage.controller;

import java.util.Arrays;
import java.util.HashMap;
import java.util.Map;

import com.lb.admin.common.utils.HttpRestClient;
import com.lb.admin.common.validator.ValidatorUtils;
import org.apache.shiro.authz.annotation.RequiresPermissions;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import com.lb.admin.modules.manage.entity.BasesettingEntity;
import com.lb.admin.modules.manage.service.BasesettingService;
import com.lb.admin.common.utils.PageUtils;
import com.lb.admin.common.utils.R;


/**
 * 基本参数设置
 *
 * @author Eric
 * @email qq857060702@gmail.com
 * @date 2019-10-06 19:22:29
 */
@RestController
@RequestMapping("manage/basesetting")
public class BasesettingController {
    @Autowired
    private BasesettingService basesettingService;

    HttpRestClient httpRestClient=new HttpRestClient();

    @Value("${rest.lb_back_end_url}")
    private String REST_URL;

    /**
     * 列表
     */
    @RequestMapping("/list")
    @RequiresPermissions("manage:basesetting:list")
    public R list(@RequestParam Map<String, Object> params) {

        PageUtils page = basesettingService.queryPage(params);

        return R.ok().put("page", page);
    }


    /**
     * 信息
     */
    @RequestMapping("/info/{id}")
    @RequiresPermissions("manage:basesetting:info")
    public R info(@PathVariable("id") Integer id) {
        BasesettingEntity basesetting = basesettingService.getById(id);

        return R.ok().put("basesetting", basesetting);
    }

    /**
     * 保存
     */
    @RequestMapping("/save")
    @RequiresPermissions("manage:basesetting:save")
    public R save(@RequestBody BasesettingEntity basesetting) {
        basesettingService.save(basesetting);


        return R.ok();
    }

    /**
     * 修改
     */
    @RequestMapping("/update")
    @RequiresPermissions("manage:basesetting:update")
    public R update(@RequestBody BasesettingEntity basesetting) {
        ValidatorUtils.validateEntity(basesetting);
        basesettingService.updateById(basesetting);

      /*  Map map = new HashMap();
        try {
            map.put("bsname", basesetting.getBsname());
            map.put("bsvalue", basesetting.getBsvalue());
            String r = HttpRestClient.postData(REST_URL + "lbapi/basesetting/update", map); // 通知后端服务更新基本配置参数
            System.out.println("通知后端服务更新基本配置参数 .. " + r);
        } catch (Exception e) {
            e.printStackTrace();
        }*/

        return R.ok();
    }

    /**
     * 删除
     */
    @RequestMapping("/delete")
    @RequiresPermissions("manage:basesetting:delete")
    public R delete(@RequestBody Integer[] ids) {
        basesettingService.removeByIds(Arrays.asList(ids));
       /* Map<String ,Object> map = new HashMap<>();
        map.put("delete",ids);
        System.err.println(map);
        System.err.println(REST_URL + "api/delete_broadcast?"+"," +map);
        httpRestClient.doPostTestTwo("api/update_broadcast",map);*/
        return R.ok();
    }

}
