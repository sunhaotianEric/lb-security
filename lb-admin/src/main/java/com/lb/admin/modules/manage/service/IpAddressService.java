package com.lb.admin.modules.manage.service;

import com.baomidou.mybatisplus.extension.service.IService;
import com.lb.admin.common.utils.PageUtils;
import com.lb.admin.modules.manage.entity.IpAddressEntity;

import java.util.Map;

/**
 * 
 *
 * @author Alex
 * @email lbjt201910@gmail.com
 * @date 2019-10-22 13:31:25
 */
public interface IpAddressService extends IService<IpAddressEntity> {

    PageUtils queryPage(Map<String, Object> params);
}

