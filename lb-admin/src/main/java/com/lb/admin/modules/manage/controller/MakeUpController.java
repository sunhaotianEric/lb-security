package com.lb.admin.modules.manage.controller;

import java.util.Arrays;
import java.util.Map;

import com.lb.admin.common.validator.ValidatorUtils;
import org.apache.shiro.authz.annotation.RequiresPermissions;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import com.lb.admin.modules.manage.entity.MakeUpEntity;
import com.lb.admin.modules.manage.service.MakeUpService;
import com.lb.admin.common.utils.PageUtils;
import com.lb.admin.common.utils.R;



/**
 * 
 *
 * @author Alex
 * @email lbjt201910@gmail.com
 * @date 2019-11-21 14:44:40
 */
@RestController
@RequestMapping("manage/makeup")
public class MakeUpController {
    @Autowired
    private MakeUpService makeUpService;

    /**
     * 列表
     */
    @RequestMapping("/list")
    @RequiresPermissions("manage:makeup:list")
    public R list(@RequestParam Map<String, Object> params){
        PageUtils page = makeUpService.queryPage(params);

        return R.ok().put("page", page);
    }


    /**
     * 信息
     */
    @RequestMapping("/info/{id}")
    @RequiresPermissions("manage:makeup:info")
    public R info(@PathVariable("id") Integer id){
        MakeUpEntity makeUp = makeUpService.getById(id);

        return R.ok().put("makeUp", makeUp);
    }

    /**
     * 保存
     */
    @RequestMapping("/save")
    @RequiresPermissions("manage:makeup:save")
    public R save(@RequestBody MakeUpEntity makeUp){
        makeUpService.save(makeUp);

        return R.ok();
    }

    /**
     * 修改
     */
    @RequestMapping("/update")
    @RequiresPermissions("manage:makeup:update")
    public R update(@RequestBody MakeUpEntity makeUp){
        ValidatorUtils.validateEntity(makeUp);
        makeUpService.updateById(makeUp);

        return R.ok();
    }

    /**
     * 删除
     */
    @RequestMapping("/delete")
    @RequiresPermissions("manage:makeup:delete")
    public R delete(@RequestBody Integer[] ids){
        makeUpService.removeByIds(Arrays.asList(ids));

        return R.ok();
    }

}
