package com.lb.admin.modules.manage.entity;

import com.baomidou.mybatisplus.annotation.TableId;
import com.baomidou.mybatisplus.annotation.TableName;
import lombok.Data;

import java.io.Serializable;
import java.util.Date;

/**
 * 
 * 
 * @author Alex
 * @email lbjt201910@gmail.com
 * @date 2019-11-21 14:44:40
 */
@Data
@TableName("lb_make_up")
public class MakeUpEntity implements Serializable {
	private static final long serialVersionUID = 1L;

	/**
	 * id
	 */
	@TableId
	private Integer id;
	/**
	 * 用户id
	 */
	private String userId;
	/**
	 * 掉单金额
	 */
	private Double dropOrderMoney;
	/**
	 * 申请时间
	 */
	private Date createTime;
	/**
	 * 0为正在审核，1为通过，2为不通过
	 */
	private Integer schedule;
	/**
	 * 申请活动名称
	 */
	private String projectId;

}
