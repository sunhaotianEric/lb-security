package com.lb.admin.modules.manage.controller;

import java.util.Arrays;
import java.util.Map;

import com.lb.admin.common.validator.ValidatorUtils;
import org.apache.shiro.authz.annotation.RequiresPermissions;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import com.lb.admin.modules.manage.entity.TopupcashLogEntity;
import com.lb.admin.modules.manage.service.TopupcashLogService;
import com.lb.admin.common.utils.PageUtils;
import com.lb.admin.common.utils.R;



/**
 * 充值提现记录
 *
 * @author Alex
 * @email lbjt201910@gmail.com
 * @date 2019-10-05 15:30:32
 */
@RestController
@RequestMapping("manage/topupcashlog")
public class TopupcashLogController {
    @Autowired
    private TopupcashLogService topupcashLogService;

    /**
     * 列表
     */
    @RequestMapping("/list")
    @RequiresPermissions("manage:topupcashlog:list")
    public R list(@RequestParam Map<String, Object> params){
        PageUtils page = topupcashLogService.queryPage(params);

        return R.ok().put("page", page);
    }


    /**
     * 信息
     */
    @RequestMapping("/info/{id}")
    @RequiresPermissions("manage:topupcashlog:info")
    public R info(@PathVariable("id") Long id){
        TopupcashLogEntity topupcashLog = topupcashLogService.getById(id);

        return R.ok().put("topupcashLog", topupcashLog);
    }

    /**
     * 保存
     */
    @RequestMapping("/save")
    @RequiresPermissions("manage:topupcashlog:save")
    public R save(@RequestBody TopupcashLogEntity topupcashLog){
        topupcashLogService.save(topupcashLog);

        return R.ok();
    }

    /**
     * 修改
     */
    @RequestMapping("/update")
    @RequiresPermissions("manage:topupcashlog:update")
    public R update(@RequestBody TopupcashLogEntity topupcashLog){
        ValidatorUtils.validateEntity(topupcashLog);
        topupcashLogService.updateById(topupcashLog);

        return R.ok();
    }

    /**
     * 删除
     */
    @RequestMapping("/delete")
    @RequiresPermissions("manage:topupcashlog:delete")
    public R delete(@RequestBody Long[] ids){
        topupcashLogService.removeByIds(Arrays.asList(ids));

        return R.ok();
    }

}
