package com.lb.admin.modules.manage.controller;

import java.util.Arrays;
import java.util.Map;

import com.lb.admin.common.validator.ValidatorUtils;
import org.apache.shiro.authz.annotation.RequiresPermissions;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import com.lb.admin.modules.manage.entity.OtherBillsEntity;
import com.lb.admin.modules.manage.service.OtherBillsService;
import com.lb.admin.common.utils.PageUtils;
import com.lb.admin.common.utils.R;



/**
 * 
 *
 * @author Alex
 * @email lbjt201910@gmail.com
 * @date 2019-11-21 13:38:27
 */
@RestController
@RequestMapping("manage/otherbills")
public class OtherBillsController {
    @Autowired
    private OtherBillsService otherBillsService;

    /**
     * 列表
     */
    @RequestMapping("/list")
    @RequiresPermissions("manage:otherbills:list")
    public R list(@RequestParam Map<String, Object> params){
        PageUtils page = otherBillsService.queryPage(params);

        return R.ok().put("page", page);
    }


    /**
     * 信息
     */
    @RequestMapping("/info/{userId}")
    @RequiresPermissions("manage:otherbills:info")
    public R info(@PathVariable("userId") String userId){
        OtherBillsEntity otherBills = otherBillsService.getById(userId);

        return R.ok().put("otherBills", otherBills);
    }

    /**
     * 保存
     */
    @RequestMapping("/save")
    @RequiresPermissions("manage:otherbills:save")
    public R save(@RequestBody OtherBillsEntity otherBills){
        otherBillsService.save(otherBills);

        return R.ok();
    }

    /**
     * 修改
     */
    @RequestMapping("/update")
    @RequiresPermissions("manage:otherbills:update")
    public R update(@RequestBody OtherBillsEntity otherBills){
        ValidatorUtils.validateEntity(otherBills);
        otherBillsService.updateById(otherBills);

        return R.ok();
    }

    /**
     * 删除
     */
    @RequestMapping("/delete")
    @RequiresPermissions("manage:otherbills:delete")
    public R delete(@RequestBody String[] userIds){
        otherBillsService.removeByIds(Arrays.asList(userIds));

        return R.ok();
    }

}
