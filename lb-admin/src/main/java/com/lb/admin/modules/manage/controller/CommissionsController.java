package com.lb.admin.modules.manage.controller;

import java.util.Arrays;
import java.util.Map;

import com.lb.admin.common.validator.ValidatorUtils;
import org.apache.shiro.authz.annotation.RequiresPermissions;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import com.lb.admin.modules.manage.entity.CommissionsEntity;
import com.lb.admin.modules.manage.service.CommissionsService;
import com.lb.admin.common.utils.PageUtils;
import com.lb.admin.common.utils.R;



/**
 * 
 *
 * @author Alex
 * @email lbjt201910@gmail.com
 * @date 2019-11-21 13:30:58
 */
@RestController
@RequestMapping("manage/commissions")
public class CommissionsController {
    @Autowired
    private CommissionsService commissionsService;

    /**
     * 列表
     */
    @RequestMapping("/list")
    @RequiresPermissions("manage:commissions:list")
    public R list(@RequestParam Map<String, Object> params){
        PageUtils page = commissionsService.queryPage(params);

        return R.ok().put("page", page);
    }


    /**
     * 信息
     */
    @RequestMapping("/info/{userId}")
    @RequiresPermissions("manage:commissions:info")
    public R info(@PathVariable("userId") String userId){
        CommissionsEntity commissions = commissionsService.getById(userId);

        return R.ok().put("commissions", commissions);
    }

    /**
     * 保存
     */
    @RequestMapping("/save")
    @RequiresPermissions("manage:commissions:save")
    public R save(@RequestBody CommissionsEntity commissions){
        commissionsService.save(commissions);

        return R.ok();
    }

    /**
     * 修改
     */
    @RequestMapping("/update")
    @RequiresPermissions("manage:commissions:update")
    public R update(@RequestBody CommissionsEntity commissions){
        ValidatorUtils.validateEntity(commissions);
        commissionsService.updateById(commissions);

        return R.ok();
    }

    /**
     * 删除
     */
    @RequestMapping("/delete")
    @RequiresPermissions("manage:commissions:delete")
    public R delete(@RequestBody String[] userIds){
        commissionsService.removeByIds(Arrays.asList(userIds));

        return R.ok();
    }

}
