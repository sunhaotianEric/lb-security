package com.lb.admin.modules.manage.controller;

import java.util.Arrays;
import java.util.List;
import java.util.Map;

import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.lb.admin.common.validator.ValidatorUtils;
import org.apache.shiro.authz.annotation.RequiresPermissions;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import com.lb.admin.modules.manage.entity.IpAddressEntity;
import com.lb.admin.modules.manage.service.IpAddressService;
import com.lb.admin.common.utils.PageUtils;
import com.lb.admin.common.utils.R;



/**
 * 
 *
 * @author eric
 * @email qq857060702@gmail.com
 * @date 2019-10-22 13:31:25
 */
@RestController
@RequestMapping("manage/ipaddress")
public class IpAddressController {
    @Autowired
    private IpAddressService ipAddressService;

    /**
     * 列表
     */
    @RequestMapping("/list")
    @RequiresPermissions("manage:ipaddress:list")
    public R list(@RequestParam Map<String, Object> params){

        PageUtils page = ipAddressService.queryPage(params);

        return R.ok().put("page", page);
    }


    /**
     * 信息
     */
    @RequestMapping("/info/{id}")
    @RequiresPermissions("manage:ipaddress:info")
    public R info(@PathVariable("id") Integer id){
        IpAddressEntity ipAddress = ipAddressService.getById(id);

        return R.ok().put("ipAddress", ipAddress);
    }

    /**
     * 保存
     */
    @RequestMapping("/save")
    @RequiresPermissions("manage:ipaddress:save")
    public R save(@RequestBody IpAddressEntity ipAddress){
        ipAddressService.save(ipAddress);

        return R.ok();
    }

    /**
     * 修改
     */
    @RequestMapping("/update")
    @RequiresPermissions("manage:ipaddress:update")
    public R update(@RequestBody IpAddressEntity ipAddress){
        ValidatorUtils.validateEntity(ipAddress);
        ipAddressService.updateById(ipAddress);

        return R.ok();
    }

    /**
     * 删除
     */
    @RequestMapping("/delete")
    @RequiresPermissions("manage:ipaddress:delete")
    public R delete(@RequestBody Integer[] ids){
        ipAddressService.removeByIds(Arrays.asList(ids));

        return R.ok();
    }

    /**
     * 通过IP查询用户信息
     */
    @RequestMapping("/select_byip")
    @RequiresPermissions("manage:ipaddress:select")
    public R selectByip(@RequestParam String ip){
        System.err.println("进入了这里");
        List<IpAddressEntity> ipAddress =ipAddressService.list(new QueryWrapper<IpAddressEntity>().eq("ip",ip));

        return R.ok().put("listByIp", ipAddress);
    }

}
