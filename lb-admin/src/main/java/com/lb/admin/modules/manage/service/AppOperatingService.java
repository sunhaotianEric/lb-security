package com.lb.admin.modules.manage.service;

import com.baomidou.mybatisplus.extension.service.IService;
import com.lb.admin.common.utils.PageUtils;
import com.lb.admin.modules.manage.entity.AppOperatingEntity;

import java.util.Map;

/**
 * 
 *
 * @author Alex
 * @email lbjt201910@gmail.com
 * @date 2019-10-15 16:09:40
 */
public interface AppOperatingService extends IService<AppOperatingEntity> {

    PageUtils queryPage(Map<String, Object> params);
}

