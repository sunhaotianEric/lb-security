package com.lb.admin.modules.manage.service;

import com.baomidou.mybatisplus.extension.service.IService;
import com.lb.admin.common.utils.PageUtils;
import com.lb.admin.modules.manage.entity.BasesettingEntity;

import java.util.Map;

/**
 * 基本参数设置
 *
 * @author Alex
 * @email lbjt201910@gmail.com
 * @date 2019-10-06 19:22:29
 */
public interface BasesettingService extends IService<BasesettingEntity> {

    PageUtils queryPage(Map<String, Object> params);
}

