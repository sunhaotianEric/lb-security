package com.lb.admin.modules.manage.dao;

import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.baomidou.mybatisplus.core.metadata.IPage;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.lb.admin.modules.manage.entity.UserGroupEntity;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import org.apache.ibatis.annotations.Mapper;
import org.apache.ibatis.annotations.Select;
import org.springframework.stereotype.Repository;

import java.util.List;
import java.util.Map;

/**
 * @author Alex
 * @email lbjt201910@gmail.com
 * @date 2019-10-07 16:39:52
 */
@Mapper
@Repository
public interface UserGroupDao extends BaseMapper<UserGroupEntity> {

    //    @Select("SELECT t1.*, (SELECT account_name FROM lb_user WHERE id = t1.user_id)userName, (SELECT name FROM lb_group WHERE id = t1.group_id)groupName, " +
//            "(SELECT value FROM sys_dict WHERE type='grouprole' AND code = t1.role)roleName FROM lb_user_group t1")
    @Select("SELECT a.* FROM (SELECT t1.*, t2.name userName, t3.name groupName, t4.value roleName FROM lb_user_group t1 LEFT JOIN lb_user t2 ON t1" +
            ".user_id=t2.id LEFT JOIN lb_group t3 ON t1.group_id=t3.id LEFT JOIN sys_dict t4 ON (t1.role=t4.code AND t4.type='grouprole')) a")
    IPage<UserGroupEntity> listPage(IPage page, QueryWrapper queryWrapper);

    @Select("SELECT t1.*, t2.name userName, t3.name groupName, t4.value roleName FROM lb_user_group t1 INNER JOIN lb_user t2 ON t1.user_id=t2.id " +
            "INNER JOIN lb_group t3 ON t1.group_id=t3.id INNER JOIN sys_dict t4 ON (t1.role=t4.code AND t4.type='grouprole') WHERE t1.id=#{id}")
    UserGroupEntity getInfoByid(Long id);
}
