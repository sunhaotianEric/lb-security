package com.lb.admin.modules.manage.entity;

import com.baomidou.mybatisplus.annotation.TableId;
import com.baomidou.mybatisplus.annotation.TableName;
import lombok.Data;

import java.io.Serializable;
import java.util.Date;

/**
 * 
 * 
 * @author Alex
 * @email lbjt201910@gmail.com
 * @date 2019-10-04 17:06:26
 */
@Data
@TableName("lb_user")
public class UserEntity implements Serializable {
	private static final long serialVersionUID = 1L;

	/**
	 * 
	 */
	@TableId
	private Integer id;
	/**
	 * 用户名
	 */
	private String accountName;
	/**
	 * 密码
	 */
	private String password;
	/**
	 * 手机号码
	 */
	private Integer phone;
	/**
	 * 名称
	 */
	private String name;
	/**
	 * 0未设置 1男 2女
	 */
	private Integer sex;
	/**
	 * 年龄
	 */
	private Integer age;
	/**
	 * 头像地址
	 */
	private String profilePicture;
	/**
	 * 创建时间
	 */
	private Date createTime;
	/**
	 * 是否禁用 0否 1是
	 */
	private Integer isForbidden;
	/**
	 * 登录时间
	 */
	private Date loginTime;
	/**
	 * 手机是否验证 0否 1是
	 */
	private Integer isCheckPhone;
	/**
	 * 金币
	 */
	private Double goldCoin;

}
