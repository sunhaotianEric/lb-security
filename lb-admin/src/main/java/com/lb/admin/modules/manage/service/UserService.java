package com.lb.admin.modules.manage.service;

import com.baomidou.mybatisplus.extension.service.IService;
import com.lb.admin.common.utils.PageUtils;
import com.lb.admin.modules.manage.entity.UserEntity;

import java.util.Map;

/**
 *
 *
 * @author Alex
 * @email lbjt201910@gmail.com
 * @date 2019-10-04 17:06:26
 */
public interface UserService extends IService<UserEntity> {

    PageUtils queryPage(Map<String, Object> params);

    void updateGoldcoin(Long id, Double money);
}

