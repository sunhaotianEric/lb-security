package com.lb.admin.modules.manage.dao;

import com.lb.admin.modules.manage.entity.TopupcashLogEntity;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import org.apache.ibatis.annotations.Mapper;

/**
 * 充值提现记录
 * 
 * @author Alex
 * @email lbjt201910@gmail.com
 * @date 2019-10-05 15:30:32
 */
@Mapper
public interface TopupcashLogDao extends BaseMapper<TopupcashLogEntity> {
	
}
