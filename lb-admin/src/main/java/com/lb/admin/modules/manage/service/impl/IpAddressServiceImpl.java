package com.lb.admin.modules.manage.service.impl;

import com.lb.admin.common.utils.Constant;
import com.lb.admin.modules.manage.entity.UserEntity;
import org.apache.commons.lang.StringUtils;
import org.springframework.stereotype.Service;
import java.util.Map;
import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.baomidou.mybatisplus.core.metadata.IPage;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.lb.admin.common.utils.PageUtils;
import com.lb.admin.common.utils.Query;

import com.lb.admin.modules.manage.dao.IpAddressDao;
import com.lb.admin.modules.manage.entity.IpAddressEntity;
import com.lb.admin.modules.manage.service.IpAddressService;


@Service("ipAddressService")
public class IpAddressServiceImpl extends ServiceImpl<IpAddressDao, IpAddressEntity> implements IpAddressService {

    @Override
    public PageUtils queryPage(Map<String, Object> params) {
        String param = (String) params.get("ip");

        IPage q = new Query<IpAddressEntity>().getPage(params);

        QueryWrapper<IpAddressEntity> qw = new QueryWrapper<IpAddressEntity>();

        if (param != null && !"".equals(param)) {

            qw.like(StringUtils.isNotBlank(param), "ip", param)

                    .or(i -> i.like(StringUtils.isNotBlank(param), "city", param))

                    .or(i -> i.like(StringUtils.isNotBlank(param), "nick_name", param))

                    .or(i -> i.like(StringUtils.isNotBlank(param), "country", param))

                    .or(i -> i.like(StringUtils.isNotBlank(param), "province", param))

                    .apply(params.get(Constant.SQL_FILTER) != null, (String) params.get(Constant.SQL_FILTER));
        }

        IPage<UserEntity> page = this.page(q, qw);

        return new PageUtils(page);
    }

}
