package com.lb.admin.modules.manage.controller;

import java.util.Arrays;
import java.util.Map;

import com.lb.admin.common.validator.ValidatorUtils;
import org.apache.shiro.authz.annotation.RequiresPermissions;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import com.lb.admin.modules.manage.entity.AnnouncementEntity;
import com.lb.admin.modules.manage.service.AnnouncementService;
import com.lb.admin.common.utils.PageUtils;
import com.lb.admin.common.utils.R;



/**
 * 
 *
 * @author Alex
 * @email lbjt201910@gmail.com
 * @date 2019-10-14 14:32:35
 */
@RestController
@RequestMapping("manage/announcement")
public class AnnouncementController {
    @Autowired
    private AnnouncementService announcementService;

    /**
     * 列表
     */
    @RequestMapping("/list")
    @RequiresPermissions("manage:announcement:list")
    public R list(@RequestParam Map<String, Object> params){
        PageUtils page = announcementService.queryPage(params);

        return R.ok().put("page", page);
    }


    /**
     * 信息
     */
    @RequestMapping("/info/{id}")
    @RequiresPermissions("manage:announcement:info")
    public R info(@PathVariable("id") Integer id){
        AnnouncementEntity announcement = announcementService.getById(id);

        return R.ok().put("announcement", announcement);
    }

    /**
     * 保存
     */
    @RequestMapping("/save")
    @RequiresPermissions("manage:announcement:save")
    public R save(@RequestBody AnnouncementEntity announcement){
        announcementService.save(announcement);

        return R.ok();
    }

    /**
     * 修改
     */
    @RequestMapping("/update")
    @RequiresPermissions("manage:announcement:update")
    public R update(@RequestBody AnnouncementEntity announcement){
        ValidatorUtils.validateEntity(announcement);
        announcementService.updateById(announcement);

        return R.ok();
    }

    /**
     * 删除
     */
    @RequestMapping("/delete")
    @RequiresPermissions("manage:announcement:delete")
    public R delete(@RequestBody Integer[] ids){
        announcementService.removeByIds(Arrays.asList(ids));

        return R.ok();
    }

}
