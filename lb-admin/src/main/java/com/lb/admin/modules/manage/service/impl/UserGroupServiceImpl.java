package com.lb.admin.modules.manage.service.impl;

import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.lb.admin.common.utils.Constant;
import org.apache.commons.lang.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.Map;

import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.baomidou.mybatisplus.core.metadata.IPage;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.lb.admin.common.utils.PageUtils;
import com.lb.admin.common.utils.Query;

import com.lb.admin.modules.manage.dao.UserGroupDao;
import com.lb.admin.modules.manage.entity.UserGroupEntity;
import com.lb.admin.modules.manage.service.UserGroupService;


@Service("userGroupService")
public class UserGroupServiceImpl extends ServiceImpl<UserGroupDao, UserGroupEntity> implements UserGroupService {

    @Override
    public PageUtils queryPage(Map<String, Object> params) {
        String param = (String) params.get("param");
        System.out.println("param:" + param);
        IPage<UserGroupEntity> ipa = new Query<UserGroupEntity>().getPage(params);
        QueryWrapper<UserGroupEntity> qw = new QueryWrapper<UserGroupEntity>();
        if (param != null && !"".equals(param)) {
            qw.like(StringUtils.isNotBlank(param), "userName", param)
                    .or(i -> i.like(StringUtils.isNotBlank(param), "groupName", param))
                    .or(i -> i.like(StringUtils.isNotBlank(param), "roleName", param))
                    .apply(params.get(Constant.SQL_FILTER) != null, (String) params.get(Constant.SQL_FILTER));
        }
//        IPage<UserGroupEntity> page = this.page(ipa, qw);
        IPage<UserGroupEntity> page = this.baseMapper.listPage(ipa, qw);
        return new PageUtils(page);
    }

    @Override
    public UserGroupEntity getInfoByid(Long id) {
        return this.baseMapper.getInfoByid(id);
    }
}
