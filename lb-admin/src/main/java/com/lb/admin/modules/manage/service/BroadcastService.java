package com.lb.admin.modules.manage.service;

import com.baomidou.mybatisplus.extension.service.IService;
import com.lb.admin.common.utils.PageUtils;
import com.lb.admin.modules.manage.entity.BroadcastEntity;

import java.util.Map;

/**
 * 
 *
 * @author Alex
 * @email lbjt201910@gmail.com
 * @date 2019-10-15 15:19:41
 */
public interface BroadcastService extends IService<BroadcastEntity> {

    PageUtils queryPage(Map<String, Object> params);
}

