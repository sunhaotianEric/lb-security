package com.lb.admin.modules.manage.entity;

import com.baomidou.mybatisplus.annotation.TableId;
import com.baomidou.mybatisplus.annotation.TableName;
import lombok.Data;

import java.io.Serializable;
import java.util.Date;

/**
 * 
 * 
 * @author Alex
 * @email lbjt201910@gmail.com
 * @date 2019-11-21 13:38:27
 */
@Data
@TableName("lb_other_bills")
public class OtherBillsEntity implements Serializable {
	private static final long serialVersionUID = 1L;

	/**
	 * 用户id
	 */
	@TableId
	private String userId;
	/**
	 * 好友id
	 */
	private String fid;
	/**
	 * 创建时间
	 */
	private Date createTime;
	/**
	 * 申请状态 0为正在审核 1成功 2为拒绝
	 */
	private Integer schedule;

}
