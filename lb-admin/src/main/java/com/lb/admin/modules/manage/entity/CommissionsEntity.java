package com.lb.admin.modules.manage.entity;

import com.baomidou.mybatisplus.annotation.TableId;
import com.baomidou.mybatisplus.annotation.TableName;
import lombok.Data;

import java.io.Serializable;
import java.util.Date;

/**
 * 
 * 
 * @author Alex
 * @email lbjt201910@gmail.com
 * @date 2019-11-21 13:30:58
 */
@Data
@TableName("lb_commissions")
public class CommissionsEntity implements Serializable {
	private static final long serialVersionUID = 1L;

	/**
	 * 用户id
	 */
	@TableId
	private String userId;
	/**
	 * 0为审核1为成功2为失败
	 */
	private Integer schedule;
	/**
	 * 创建时间
	 */
	private Date createTime;

}
