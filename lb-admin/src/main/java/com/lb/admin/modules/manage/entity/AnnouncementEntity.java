package com.lb.admin.modules.manage.entity;

import com.baomidou.mybatisplus.annotation.TableId;
import com.baomidou.mybatisplus.annotation.TableName;
import lombok.Data;

import java.io.Serializable;
import java.util.Date;

/**
 * 
 * 
 * @author Alex
 * @email lbjt201910@gmail.com
 * @date 2019-10-14 14:32:35
 */
@Data
@TableName("lb_announcement")
public class AnnouncementEntity implements Serializable {
	private static final long serialVersionUID = 1L;

	/**
	 * 公告id
	 */
	@TableId
	private Integer id;
	/**
	 * 公告id
	 */
	private Integer noticeId;
	/**
	 * 公告内容
	 */
	private String content;
	/**
	 * 公告创建时间
	 */
	private Date createTime;

}
