package com.lb.admin.modules.manage.entity;

import com.baomidou.mybatisplus.annotation.TableId;
import com.baomidou.mybatisplus.annotation.TableName;
import lombok.Data;

import java.io.Serializable;
import java.util.Date;

/**
 * 充值提现记录
 * 
 * @author Alex
 * @email lbjt201910@gmail.com
 * @date 2019-10-05 15:30:32
 */
@Data
@TableName("lb_topupcash_log")
public class TopupcashLogEntity implements Serializable {
	private static final long serialVersionUID = 1L;

	/**
	 * 充值提现id
	 */
	@TableId
	private Long id;
	/**
	 * 用户id
	 */
	private Long userid;
	/**
	 * 用户名
	 */
	private String username;
	/**
	 * 用户昵称
	 */
	private String nickname;
	/**
	 * 类型 0:保留，1:充值，2:提现
	 */
	private Integer type;
	/**
	 * 金额
	 */
	private Double money;
	/**
	 * 备注
	 */
	private String remark;
	/**
	 * 操作人id
	 */
	private Long operater;
	/**
	 * 操作人姓名
	 */
	private String operatername;
	/**
	 * 操作时间
	 */
	private Date operatetime;

}
