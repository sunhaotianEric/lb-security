package com.lb.admin.modules.manage.service;

import com.baomidou.mybatisplus.extension.service.IService;
import com.lb.admin.common.utils.PageUtils;
import com.lb.admin.modules.manage.entity.UserGroupEntity;

import java.util.Map;

/**
 *
 *
 * @author Alex
 * @email lbjt201910@gmail.com
 * @date 2019-10-07 16:39:52
 */
public interface UserGroupService extends IService<UserGroupEntity> {

    PageUtils queryPage(Map<String, Object> params);

    UserGroupEntity getInfoByid(Long id);
}

