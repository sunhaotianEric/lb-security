package com.lb.admin.modules.manage.entity;

import com.baomidou.mybatisplus.annotation.TableField;
import com.baomidou.mybatisplus.annotation.TableId;
import com.baomidou.mybatisplus.annotation.TableName;
import lombok.Data;

import java.io.Serializable;
import java.util.Date;

/**
 * 
 * 
 * @author Alex
 * @email lbjt201910@gmail.com
 * @date 2019-10-15 15:19:41
 */
@Data
@TableName("lb_broadcast")
public class BroadcastEntity implements Serializable {
	private static final long serialVersionUID = 1L;

	/**
	 * 广播id
	 */
	@TableId
	private Integer id;


	@TableField(exist=false)
	Integer groupId; // 群组id
	/**
	 * 广播内容
	 */
	private String content;
	/**
	 * 状态：0使用 1关闭
	 */
	private Integer way;
	/**
	 * 广播间隔
	 */
	private Long time;

}
