package com.lb.admin.modules.manage.controller;

import java.util.Arrays;
import java.util.Map;

import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.lb.admin.common.validator.ValidatorUtils;
import org.apache.shiro.authz.annotation.RequiresPermissions;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import com.lb.admin.modules.manage.entity.UserGroupEntity;
import com.lb.admin.modules.manage.service.UserGroupService;
import com.lb.admin.common.utils.PageUtils;
import com.lb.admin.common.utils.R;



/**
 *
 *
 * @author Alex
 * @email lbjt201910@gmail.com
 * @date 2019-10-07 16:39:52
 */
@RestController
@RequestMapping("manage/usergroup")
public class UserGroupController {
    @Autowired
    private UserGroupService userGroupService;

    /**
     * 列表
     */
    @RequestMapping("/list")
    @RequiresPermissions("manage:usergroup:list")
    public R list(@RequestParam Map<String, Object> params){
        PageUtils page = userGroupService.queryPage(params);

        return R.ok().put("page", page);
    }


    /**
     * 信息
     */
    @RequestMapping("/info/{id}")
    @RequiresPermissions("manage:usergroup:info")
    public R info(@PathVariable("id") Long id){
        UserGroupEntity userGroup = userGroupService.getInfoByid(id);

        return R.ok().put("userGroup", userGroup);
    }

    /**
     * 保存
     */
    @RequestMapping("/save")
    @RequiresPermissions("manage:usergroup:save")
    public R save(@RequestBody UserGroupEntity userGroup){
        userGroupService.save(userGroup);

        return R.ok();
    }

    /**
     * 修改
     */
    @RequestMapping("/update")
    @RequiresPermissions("manage:usergroup:update")
    public R update(@RequestBody UserGroupEntity userGroup){
        ValidatorUtils.validateEntity(userGroup);
        userGroupService.updateById(userGroup);

        return R.ok();
    }

    /**
     * 删除
     */
    @RequestMapping("/delete")
    @RequiresPermissions("manage:usergroup:delete")
    public R delete(@RequestBody Integer[] ids){
        userGroupService.removeByIds(Arrays.asList(ids));

        return R.ok();
    }

}
