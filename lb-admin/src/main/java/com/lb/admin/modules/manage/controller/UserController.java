package com.lb.admin.modules.manage.controller;

import java.util.Arrays;
import java.util.Date;
import java.util.Map;

import com.lb.admin.common.validator.ValidatorUtils;
import com.lb.admin.modules.manage.entity.TopupcashLogEntity;
import com.lb.admin.modules.manage.service.TopupcashLogService;
import org.apache.shiro.authz.annotation.RequiresPermissions;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import com.lb.admin.modules.manage.entity.UserEntity;
import com.lb.admin.modules.manage.service.UserService;
import com.lb.admin.common.utils.PageUtils;
import com.lb.admin.common.utils.R;


/**
 * @author Alex
 * @email lbjt201910@gmail.com
 * @date 2019-10-04 17:06:26
 */
@RestController
@RequestMapping("manage/user")
public class UserController {
    @Autowired
    private UserService userService;

    @Autowired
    private TopupcashLogService tcLogService; // 充值提现记录

    /**
     * 列表
     */
    @RequestMapping("/list")
    @RequiresPermissions("manage:user:list")
    public R list(@RequestParam Map<String, Object> params) {
        PageUtils page = userService.queryPage(params);
        return R.ok().put("page", page);
    }


    /**
     * 信息
     */
    @RequestMapping("/info/{id}")
    @RequiresPermissions("manage:user:info")
    public R info(@PathVariable("id") Integer id) {
        UserEntity user = userService.getById(id);
        return R.ok().put("user", user);
    }

    /**
     * 保存
     */
    @RequestMapping("/save")
    @RequiresPermissions("manage:user:save")
    public R save(@RequestBody UserEntity user) {
        userService.save(user);
        return R.ok();
    }

    /**
     * 修改
     */
    @RequestMapping("/update")
    @RequiresPermissions("manage:user:update")
    public R update(@RequestBody UserEntity user) {
        ValidatorUtils.validateEntity(user);
        userService.updateById(user);
        return R.ok();
    }

    /**
     * 删除
     */
    @RequestMapping("/delete")
    @RequiresPermissions("manage:user:delete")
    public R delete(@RequestBody Integer[] ids) {
        userService.removeByIds(Arrays.asList(ids));
        return R.ok();
    }

    /**
     * 充值
     */
    @RequestMapping("/topup")
    @RequiresPermissions("manage:user:topup")
    public R topup(@RequestBody TopupcashLogEntity tclEntity) throws Exception {
        System.out.println(tclEntity.toString());
//        TopupcashLogEntity tclEntity = new TopupcashLogEntity();
//        tclEntity.setUserid(Long.parseLong("" + param.get("userid")));
//        tclEntity.setUsername("" + param.get("username"));
//        tclEntity.setNickname("" + param.get("nickname"));
//        tclEntity.setType(1); // 充值
//        tclEntity.setMoney(money);
//        tclEntity.setRemark("" + param.get("remark"));
//        tclEntity.setOperater(Long.parseLong("" + param.get("operater")));
//        tclEntity.setOperatername("" + param.get("operatername"));
        tclEntity.setOperatetime(new Date());
//        ValidatorUtils.validateEntity(tclEntity);
        userService.updateGoldcoin(tclEntity.getUserid(), tclEntity.getMoney()); // 加上金额
        tcLogService.save(tclEntity);
        return R.ok();
    }

    /**
     * 取现
     */
    @RequestMapping("/cash")
    @RequiresPermissions("manage:user:cash")
    public R cash(@RequestBody TopupcashLogEntity tclEntity) {
        tclEntity.setOperatetime(new Date());
        userService.updateGoldcoin(tclEntity.getUserid(), -tclEntity.getMoney()); // 减去金额
        tcLogService.save(tclEntity);
        return R.ok();
    }


}
