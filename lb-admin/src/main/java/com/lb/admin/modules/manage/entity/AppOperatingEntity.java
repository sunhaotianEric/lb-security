package com.lb.admin.modules.manage.entity;

import com.baomidou.mybatisplus.annotation.TableId;
import com.baomidou.mybatisplus.annotation.TableName;
import lombok.Data;

import java.io.Serializable;
import java.util.Date;

/**
 * 
 * 
 * @author Alex
 * @email lbjt201910@gmail.com
 * @date 2019-10-15 16:09:40
 */
@Data
@TableName("lb_app_operating")
public class AppOperatingEntity implements Serializable {
	private static final long serialVersionUID = 1L;

	/**
	 * 应用id
	 */
	@TableId
	private Integer id;
	/**
	 * 应用名称
	 */
	private String appName;
	/**
	 * 打开方式
	 */
	private String openTheWay;
	/**
	 * 状态 0为开启1为关闭
	 */
	private Integer statu;

}
