package com.lb.admin.modules.manage.service.impl;

import org.springframework.stereotype.Service;
import java.util.Map;
import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.baomidou.mybatisplus.core.metadata.IPage;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.lb.admin.common.utils.PageUtils;
import com.lb.admin.common.utils.Query;

import com.lb.admin.modules.manage.dao.BroadcastDao;
import com.lb.admin.modules.manage.entity.BroadcastEntity;
import com.lb.admin.modules.manage.service.BroadcastService;


@Service("broadcastService")
public class BroadcastServiceImpl extends ServiceImpl<BroadcastDao, BroadcastEntity> implements BroadcastService {

    @Override
    public PageUtils queryPage(Map<String, Object> params) {
        IPage<BroadcastEntity> page = this.page(
                new Query<BroadcastEntity>().getPage(params),
                new QueryWrapper<BroadcastEntity>()
        );

        return new PageUtils(page);
    }

}
