package com.lb.admin.modules.manage.service.impl;

import org.springframework.stereotype.Service;
import java.util.Map;
import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.baomidou.mybatisplus.core.metadata.IPage;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.lb.admin.common.utils.PageUtils;
import com.lb.admin.common.utils.Query;

import com.lb.admin.modules.manage.dao.OtherBillsDao;
import com.lb.admin.modules.manage.entity.OtherBillsEntity;
import com.lb.admin.modules.manage.service.OtherBillsService;


@Service("otherBillsService")
public class OtherBillsServiceImpl extends ServiceImpl<OtherBillsDao, OtherBillsEntity> implements OtherBillsService {

    @Override
    public PageUtils queryPage(Map<String, Object> params) {
        IPage<OtherBillsEntity> page = this.page(
                new Query<OtherBillsEntity>().getPage(params),
                new QueryWrapper<OtherBillsEntity>()
        );

        return new PageUtils(page);
    }

}
