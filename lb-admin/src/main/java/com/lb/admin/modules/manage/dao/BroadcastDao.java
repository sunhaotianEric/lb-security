package com.lb.admin.modules.manage.dao;

import com.lb.admin.modules.manage.entity.BroadcastEntity;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import org.apache.ibatis.annotations.Mapper;

/**
 * 
 * 
 * @author Alex
 * @email lbjt201910@gmail.com
 * @date 2019-10-15 15:19:41
 */
@Mapper
public interface BroadcastDao extends BaseMapper<BroadcastEntity> {
	
}
