package com.lb.admin.modules.manage.dao;

import com.lb.admin.modules.manage.entity.IpAddressEntity;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import org.apache.ibatis.annotations.Mapper;
import org.apache.ibatis.annotations.Select;

/**
 * 
 * 
 * @author Eric
 * @email qq857060702@gmail.com
 * @date 2019-10-22 13:31:25
 */
@Mapper
public interface IpAddressDao extends BaseMapper<IpAddressEntity> {

	
}
