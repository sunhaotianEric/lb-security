package com.lb.admin.modules.manage.controller;

import java.util.Arrays;
import java.util.HashMap;
import java.util.Map;

import com.alibaba.fastjson.JSON;
import com.lb.admin.common.utils.HttpRestClient;
import com.lb.admin.common.validator.ValidatorUtils;
import org.apache.shiro.authz.annotation.RequiresPermissions;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;
import com.lb.admin.modules.manage.entity.BroadcastEntity;
import com.lb.admin.modules.manage.service.BroadcastService;
import com.lb.admin.common.utils.PageUtils;
import com.lb.admin.common.utils.R;



/**
 * 
 *
 * @author eric
 * @email qq857060702@gmail.com
 * @date 2019-10-15 15:19:41
 */
@RestController
@RequestMapping("manage/broadcast")
public class BroadcastController {
    @Autowired
    private BroadcastService broadcastService;

    @Value("${rest.lb_back_end_url}")
    private String REST_URL;

    HttpRestClient httpRestClient=new HttpRestClient();
    /**
     * 列表
     */
    @RequestMapping("/list")
    @RequiresPermissions("manage:broadcast:list")
    public R list(@RequestParam Map<String, Object> params){

        PageUtils page = broadcastService.queryPage(params);

        return R.ok().put("page", page);
    }


    /**
     * 信息
     */
    @RequestMapping("/info/{id}")
    @RequiresPermissions("manage:broadcast:info")
    public R info(@PathVariable("id") Integer id){
        BroadcastEntity broadcast = broadcastService.getById(id);
        httpRestClient.doPostTestTwo(REST_URL+"api/del_broadcast",null);

        return R.ok().put("broadcast", broadcast);
    }

    /**
     * 保存
     */
    @RequestMapping("/save")
    @RequiresPermissions("manage:broadcast:save")
    public R save(@RequestBody BroadcastEntity broadcast){
        broadcastService.save(broadcast);

        notice(broadcast);
        httpRestClient.doPostTestTwo(REST_URL+"api/del_broadcast",null);
        return R.ok();
    }



    /**
     * 修改
     */
    @RequestMapping("/update")
    @RequiresPermissions("manage:broadcast:update")
    public R update(@RequestBody BroadcastEntity broadcast){
        ValidatorUtils.validateEntity(broadcast);
        broadcastService.updateById(broadcast);
        notice(broadcast);
        httpRestClient.doPostTestTwo(REST_URL+"api/del_broadcast",null);
        return R.ok();
    }

    /**
     * 删除
     */
    @RequestMapping("/delete")
    @RequiresPermissions("manage:broadcast:delete")
    public R delete(@RequestBody Integer[] ids){
        broadcastService.removeByIds(Arrays.asList(ids));
        Map<String ,Object> map = new HashMap<>();
        map.put("delete",ids);
        System.err.println(map);

        httpRestClient.doPostTestTwo(REST_URL+"api/del_broadcast",map);

        return R.ok();
    }


    public void notice(BroadcastEntity broadcast){
        Map<String,Object> map = new HashMap<>();
            map.put("broadcast", broadcast);
            System.err.println(map);
            httpRestClient.doPostTestTwo(REST_URL+"api/update_broadcast",map);

    }

}
