package com.lb.admin.modules.manage.controller;

import java.util.Arrays;
import java.util.Map;

import com.lb.admin.common.validator.ValidatorUtils;
import org.apache.shiro.authz.annotation.RequiresPermissions;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import com.lb.admin.modules.manage.entity.AppOperatingEntity;
import com.lb.admin.modules.manage.service.AppOperatingService;
import com.lb.admin.common.utils.PageUtils;
import com.lb.admin.common.utils.R;



/**
 * 
 *
 * @author eric
 * @email qq857060702@gmail.com
 * @date 2019-10-15 16:09:40
 */
@RestController
@RequestMapping("manage/appoperating")
public class AppOperatingController {
    @Autowired
    private AppOperatingService appOperatingService;

    /**
     * 列表
     */
    @RequestMapping("/list")
    @RequiresPermissions("manage:appoperating:list")
    public R list(@RequestParam Map<String, Object> params){
        PageUtils page = appOperatingService.queryPage(params);

        return R.ok().put("page", page);
    }


    /**
     * 信息
     */
    @RequestMapping("/info/{id}")
    @RequiresPermissions("manage:appoperating:info")
    public R info(@PathVariable("id") Integer id){
        AppOperatingEntity appOperating = appOperatingService.getById(id);

        return R.ok().put("appOperating", appOperating);
    }

    /**
     * 保存
     */
    @RequestMapping("/save")
    @RequiresPermissions("manage:appoperating:save")
    public R save(@RequestBody AppOperatingEntity appOperating){
        appOperatingService.save(appOperating);

        return R.ok();
    }

    /**
     * 修改
     */
    @RequestMapping("/update")
    @RequiresPermissions("manage:appoperating:update")
    public R update(@RequestBody AppOperatingEntity appOperating){
        ValidatorUtils.validateEntity(appOperating);
        appOperatingService.updateById(appOperating);

        return R.ok();
    }

    /**
     * 删除
     */
    @RequestMapping("/delete")
    @RequiresPermissions("manage:appoperating:delete")
    public R delete(@RequestBody Integer[] ids){
        appOperatingService.removeByIds(Arrays.asList(ids));

        return R.ok();
    }

}
