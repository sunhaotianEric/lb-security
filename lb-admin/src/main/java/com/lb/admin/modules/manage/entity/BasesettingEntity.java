package com.lb.admin.modules.manage.entity;

import com.baomidou.mybatisplus.annotation.TableId;
import com.baomidou.mybatisplus.annotation.TableName;
import lombok.Data;

import java.io.Serializable;
import java.util.Date;

/**
 * 基本参数设置
 * 
 * @author Alex
 * @email lbjt201910@gmail.com
 * @date 2019-10-06 19:22:29
 */
@Data
@TableName("lb_basesetting")
public class BasesettingEntity implements Serializable {
	private static final long serialVersionUID = 1L;

	/**
	 * 
	 */
	@TableId
	private Integer id;
	/**
	 * 参数名称
	 */
	private String bsname;
	/**
	 * 参数值
	 */
	private String bsvalue;
	/**
	 * 备注
	 */
	private String remark;
	/**
	 * 参数说明
	 */
	private String bsexplain;

}
