package com.lb.admin.modules.manage.service.impl;

import org.springframework.stereotype.Service;
import java.util.Map;
import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.baomidou.mybatisplus.core.metadata.IPage;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.lb.admin.common.utils.PageUtils;
import com.lb.admin.common.utils.Query;

import com.lb.admin.modules.manage.dao.BasesettingDao;
import com.lb.admin.modules.manage.entity.BasesettingEntity;
import com.lb.admin.modules.manage.service.BasesettingService;


@Service("basesettingService")
public class BasesettingServiceImpl extends ServiceImpl<BasesettingDao, BasesettingEntity> implements BasesettingService {

    @Override
    public PageUtils queryPage(Map<String, Object> params) {
        IPage<BasesettingEntity> page = this.page(
                new Query<BasesettingEntity>().getPage(params),
                new QueryWrapper<BasesettingEntity>()
        );

        return new PageUtils(page);
    }

}
