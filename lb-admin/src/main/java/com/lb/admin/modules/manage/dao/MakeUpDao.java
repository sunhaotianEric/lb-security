package com.lb.admin.modules.manage.dao;

import com.lb.admin.modules.manage.entity.MakeUpEntity;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import org.apache.ibatis.annotations.Mapper;

/**
 * 
 * 
 * @author Alex
 * @email lbjt201910@gmail.com
 * @date 2019-11-21 14:44:40
 */
@Mapper
public interface MakeUpDao extends BaseMapper<MakeUpEntity> {
	
}
