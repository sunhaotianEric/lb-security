package com.lb.admin.modules.manage.service.impl;

import org.springframework.stereotype.Service;
import java.util.Map;
import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.baomidou.mybatisplus.core.metadata.IPage;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.lb.admin.common.utils.PageUtils;
import com.lb.admin.common.utils.Query;

import com.lb.admin.modules.manage.dao.AppOperatingDao;
import com.lb.admin.modules.manage.entity.AppOperatingEntity;
import com.lb.admin.modules.manage.service.AppOperatingService;


@Service("appOperatingService")
public class AppOperatingServiceImpl extends ServiceImpl<AppOperatingDao, AppOperatingEntity> implements AppOperatingService {

    @Override
    public PageUtils queryPage(Map<String, Object> params) {
        IPage<AppOperatingEntity> page = this.page(
                new Query<AppOperatingEntity>().getPage(params),
                new QueryWrapper<AppOperatingEntity>()
        );

        return new PageUtils(page);
    }

}
