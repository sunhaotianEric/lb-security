package com.lb.admin.modules.manage.entity;

import com.baomidou.mybatisplus.annotation.TableId;
import com.baomidou.mybatisplus.annotation.TableName;
import lombok.Data;

import java.io.Serializable;
import java.util.Date;

/**
 * 
 * 
 * @author Alex
 * @email lbjt201910@gmail.com
 * @date 2019-10-22 13:31:25
 */
@Data
@TableName("lb_ip_address")
public class IpAddressEntity implements Serializable {
	private static final long serialVersionUID = 1L;

	/**
	 * 用户id
	 */
	@TableId
	private Integer id;
	/**
	 * 用户访问IP
	 */
	private String ip;
	/**
	 * 城市
	 */
	private String city;
	/**
	 * 省份
	 */
	private String province;
	/**
	 * 国家
	 */
	private String country;
	/**
	 * 用户昵称
	 */
	private String nickName;
	/**
	 * 登陆时间
	 */
	private Date loginTime;

}
