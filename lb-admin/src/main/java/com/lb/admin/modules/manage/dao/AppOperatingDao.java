package com.lb.admin.modules.manage.dao;

import com.lb.admin.modules.manage.entity.AppOperatingEntity;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import org.apache.ibatis.annotations.Mapper;

/**
 * 
 * 
 * @author Alex
 * @email lbjt201910@gmail.com
 * @date 2019-10-15 16:09:40
 */
@Mapper
public interface AppOperatingDao extends BaseMapper<AppOperatingEntity> {
	
}
