package com.lb.admin.modules.manage.service.impl;

import org.springframework.stereotype.Service;
import java.util.Map;
import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.baomidou.mybatisplus.core.metadata.IPage;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.lb.admin.common.utils.PageUtils;
import com.lb.admin.common.utils.Query;

import com.lb.admin.modules.manage.dao.AnnouncementDao;
import com.lb.admin.modules.manage.entity.AnnouncementEntity;
import com.lb.admin.modules.manage.service.AnnouncementService;


@Service("announcementService")
public class AnnouncementServiceImpl extends ServiceImpl<AnnouncementDao, AnnouncementEntity> implements AnnouncementService {

    @Override
    public PageUtils queryPage(Map<String, Object> params) {
        IPage<AnnouncementEntity> page = this.page(
                new Query<AnnouncementEntity>().getPage(params),
                new QueryWrapper<AnnouncementEntity>()
        );

        return new PageUtils(page);
    }

}
