/**
 * Copyright (c) 2016-2019 人人开源 All rights reserved.
 *
 * https://www.renren.io
 *
 * 版权所有，侵权必究！
 */

package com.lb.admin.modules.sys.service;

import com.baomidou.mybatisplus.extension.service.IService;
import com.lb.admin.common.utils.PageUtils;
import com.lb.admin.modules.sys.entity.SysDictEntity;

import java.util.List;
import java.util.Map;

/**
 * 数据字典
 *
 * @author Mark sunlightcs@gmail.com
 */
public interface SysDictService extends IService<SysDictEntity> {

    PageUtils queryPage(Map<String, Object> params);

    List<SysDictEntity> listByType(String type);
}

