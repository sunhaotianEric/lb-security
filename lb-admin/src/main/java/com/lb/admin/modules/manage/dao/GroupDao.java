package com.lb.admin.modules.manage.dao;

import com.lb.admin.modules.manage.entity.GroupEntity;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import org.apache.ibatis.annotations.Mapper;

/**
 * 
 * 
 * @author Alex
 * @email lbjt201910@gmail.com
 * @date 2019-10-07 13:05:02
 */
@Mapper
public interface GroupDao extends BaseMapper<GroupEntity> {
	
}
