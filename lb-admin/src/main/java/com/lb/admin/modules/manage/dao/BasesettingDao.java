package com.lb.admin.modules.manage.dao;

import com.lb.admin.modules.manage.entity.BasesettingEntity;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import org.apache.ibatis.annotations.Mapper;

/**
 * 基本参数设置
 * 
 * @author Alex
 * @email lbjt201910@gmail.com
 * @date 2019-10-06 19:22:29
 */
@Mapper
public interface BasesettingDao extends BaseMapper<BasesettingEntity> {
	
}
