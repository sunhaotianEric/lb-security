package com.lb.admin.modules.manage.service.impl;

import com.lb.admin.common.utils.Constant;
import com.lb.admin.modules.sys.entity.SysUserEntity;
import org.apache.commons.lang.StringUtils;
import org.springframework.stereotype.Service;

import java.util.Map;

import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.baomidou.mybatisplus.core.metadata.IPage;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.lb.admin.common.utils.PageUtils;
import com.lb.admin.common.utils.Query;

import com.lb.admin.modules.manage.dao.UserDao;
import com.lb.admin.modules.manage.entity.UserEntity;
import com.lb.admin.modules.manage.service.UserService;
import org.springframework.transaction.annotation.Transactional;


@Service("userService")
public class UserServiceImpl extends ServiceImpl<UserDao, UserEntity> implements UserService {

    @Override
    public PageUtils queryPage(Map<String, Object> params) {
        String param = (String) params.get("username");
        IPage q = new Query<UserEntity>().getPage(params);
        QueryWrapper<UserEntity> qw = new QueryWrapper<UserEntity>();
        if (param != null && !"".equals(param)) {
            qw.like(StringUtils.isNotBlank(param), "account_name", param)
                    .or(i -> i.like(StringUtils.isNotBlank(param), "name", param))
                    .or(i -> i.like(StringUtils.isNotBlank(param), "phone", param))
                    .apply(params.get(Constant.SQL_FILTER) != null, (String) params.get(Constant.SQL_FILTER));
        }

        IPage<UserEntity> page = this.page(q, qw);

        return new PageUtils(page);
    }

    @Override
    @Transactional(rollbackFor = Exception.class)
    public void updateGoldcoin(Long id, Double money) {
        baseMapper.updateGoldcoin(id, money);
    }

}
