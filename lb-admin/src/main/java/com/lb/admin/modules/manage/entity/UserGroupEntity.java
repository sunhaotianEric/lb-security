package com.lb.admin.modules.manage.entity;

import com.baomidou.mybatisplus.annotation.TableField;
import com.baomidou.mybatisplus.annotation.TableId;
import com.baomidou.mybatisplus.annotation.TableName;
import lombok.Data;

import java.io.Serializable;
import java.util.Date;

/**
 *
 *
 * @author Alex
 * @email lbjt201910@gmail.com
 * @date 2019-10-07 16:39:52
 */
@Data
@TableName("lb_user_group")
public class UserGroupEntity implements Serializable {
	private static final long serialVersionUID = 1L;

	/**
	 *
	 */
	@TableId
	private Long id;

	/**
	 * 用户id
	 */
	private Integer userId;
	/**
	 * 用户名称
	 */
	@TableField(exist=false)
	private String userName;
	/**
	 * 群组id
	 */
	private Integer groupId;
	/**
	 * 群组名称
	 */
	@TableField(exist=false)
	private String groupName;
	/**
	 * 在群昵称
	 */
	private String nick;
	/**
	 * 在群角色 1群主 2管理员 3成员 4游客
	 */
	private Integer role;
	/**
	 * 角色名称
	 */
	@TableField(exist=false)
	private String roleName;

}
