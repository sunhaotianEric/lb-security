package com.lb.admin.modules.manage.service.impl;

import com.lb.admin.common.utils.Constant;
import org.apache.commons.lang.StringUtils;
import org.springframework.stereotype.Service;
import java.util.Map;
import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.baomidou.mybatisplus.core.metadata.IPage;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.lb.admin.common.utils.PageUtils;
import com.lb.admin.common.utils.Query;

import com.lb.admin.modules.manage.dao.TopupcashLogDao;
import com.lb.admin.modules.manage.entity.TopupcashLogEntity;
import com.lb.admin.modules.manage.service.TopupcashLogService;


@Service("topupcashLogService")
public class TopupcashLogServiceImpl extends ServiceImpl<TopupcashLogDao, TopupcashLogEntity> implements TopupcashLogService {

    @Override
    public PageUtils queryPage(Map<String, Object> params) {
        String param = (String) params.get("username");
        IPage q = new Query<TopupcashLogEntity>().getPage(params);
        QueryWrapper<TopupcashLogEntity> qw = new QueryWrapper<TopupcashLogEntity>();
        if (param != null && !"".equals(param)) {
            qw.like(StringUtils.isNotBlank(param), "username", param)
                    .or(i -> i.like(StringUtils.isNotBlank(param), "nickname", param))
                    .or(i -> i.like(StringUtils.isNotBlank(param), "operatername", param))
                    .or(i -> i.like(StringUtils.isNotBlank(param), "remark", param))
                    .apply(params.get(Constant.SQL_FILTER) != null, (String) params.get(Constant.SQL_FILTER));
        }

        IPage<TopupcashLogEntity> page = this.page(q, qw);

        return new PageUtils(page);
    }

}
