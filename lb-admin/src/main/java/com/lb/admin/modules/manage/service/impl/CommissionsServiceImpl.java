package com.lb.admin.modules.manage.service.impl;

import org.springframework.stereotype.Service;
import java.util.Map;
import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.baomidou.mybatisplus.core.metadata.IPage;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.lb.admin.common.utils.PageUtils;
import com.lb.admin.common.utils.Query;

import com.lb.admin.modules.manage.dao.CommissionsDao;
import com.lb.admin.modules.manage.entity.CommissionsEntity;
import com.lb.admin.modules.manage.service.CommissionsService;


@Service("commissionsService")
public class CommissionsServiceImpl extends ServiceImpl<CommissionsDao, CommissionsEntity> implements CommissionsService {

    @Override
    public PageUtils queryPage(Map<String, Object> params) {
        IPage<CommissionsEntity> page = this.page(
                new Query<CommissionsEntity>().getPage(params),
                new QueryWrapper<CommissionsEntity>()
        );

        return new PageUtils(page);
    }

}
