package com.lb.admin.modules.manage.service;

import com.baomidou.mybatisplus.extension.service.IService;
import com.lb.admin.common.utils.PageUtils;
import com.lb.admin.modules.manage.entity.OtherBillsEntity;

import java.util.Map;

/**
 * 
 *
 * @author Alex
 * @email lbjt201910@gmail.com
 * @date 2019-11-21 13:38:27
 */
public interface OtherBillsService extends IService<OtherBillsEntity> {

    PageUtils queryPage(Map<String, Object> params);
}

