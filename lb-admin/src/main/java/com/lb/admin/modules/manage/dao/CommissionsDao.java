package com.lb.admin.modules.manage.dao;

import com.lb.admin.modules.manage.entity.CommissionsEntity;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import org.apache.ibatis.annotations.Mapper;

/**
 * 
 * 
 * @author Alex
 * @email lbjt201910@gmail.com
 * @date 2019-11-21 13:30:58
 */
@Mapper
public interface CommissionsDao extends BaseMapper<CommissionsEntity> {
	
}
