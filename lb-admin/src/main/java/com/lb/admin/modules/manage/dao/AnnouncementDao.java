package com.lb.admin.modules.manage.dao;

import com.lb.admin.modules.manage.entity.AnnouncementEntity;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import org.apache.ibatis.annotations.Mapper;

/**
 * 
 * 
 * @author Alex
 * @email lbjt201910@gmail.com
 * @date 2019-10-14 14:32:35
 */
@Mapper
public interface AnnouncementDao extends BaseMapper<AnnouncementEntity> {
	
}
