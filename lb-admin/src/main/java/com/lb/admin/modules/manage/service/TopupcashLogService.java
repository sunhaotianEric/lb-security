package com.lb.admin.modules.manage.service;

import com.baomidou.mybatisplus.extension.service.IService;
import com.lb.admin.common.utils.PageUtils;
import com.lb.admin.modules.manage.entity.TopupcashLogEntity;

import java.util.Map;

/**
 * 充值提现记录
 *
 * @author Alex
 * @email lbjt201910@gmail.com
 * @date 2019-10-05 15:30:32
 */
public interface TopupcashLogService extends IService<TopupcashLogEntity> {

    PageUtils queryPage(Map<String, Object> params);
}

