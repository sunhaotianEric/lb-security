$(function () {
    $("#jqGrid").jqGrid({
        url: baseURL + 'manage/user/list',
        datatype: "json",
        colModel: [
            {label: 'id', name: 'id', index: 'id', width: 30, key: true}, //, formatter:operateFormatter
            {label: '用户名', name: 'accountName', index: 'account_name', width: 100},
            // { label: '密码', name: 'password', index: 'password', width: 80 },
            {label: '手机号码', name: 'phone', index: 'phone', width: 120},
            {label: '名称', name: 'name', index: 'name', width: 100},
            {label: '性别', name: 'sex', index: 'sex', width: 50, formatter: ageFormatter},
            {label: '年龄', name: 'age', index: 'age', width: 50},
            {label: '头像', name: 'profilePicture', index: 'profile_picture', width: 50, formatter: imageFormatter},
            {label: '创建时间', name: 'createTime', index: 'create_time', width: 120},
            {label: '启用/禁用', name: 'isForbidden', index: 'is_forbidden', width: 50, formatter: forbiddenFormatter},
            {label: '登录时间', name: 'loginTime', index: 'login_time', width: 120},
            {label: '手机验证', name: 'isCheckPhone', index: 'is_check_phone', width: 50, formatter: phoneverifyFormatter},
            {label: '金币', name: 'goldCoin', index: 'gold_coin', width: 60}
        ],
        viewrecords: true,
        height: 385,
        rowNum: 10,
        rowList: [10, 30, 50],
        rownumbers: true,
        rownumWidth: 25,
        autowidth: true,
        multiselect: true,
        pager: "#jqGridPager",
        jsonReader: {
            root: "page.list",
            page: "page.currPage",
            total: "page.totalPage",
            records: "page.totalCount"
        },
        prmNames: {
            page: "page",
            rows: "limit",
            order: "order"
        },
        gridComplete: function () {
            //隐藏grid底部滚动条
            $("#jqGrid").closest(".ui-jqgrid-bdiv").css({"overflow-x": "hidden"});
        }
    });
});

function operateFormatter(cellvalue, options, rowdata) {
    var html = '<a title="充值" @click="topup"><span class="label label-warning">充值</span></a> ';
    html += '<a title="提现" @click="cash"><span class="label label-info">提现</span></a>';
    return html;
}

function ageFormatter(cellvalue, options, rowdata) {
    if (cellvalue != null) {
        if (cellvalue == 1) {
            return '男';
        } else if (cellvalue == 2) {
            return '女';
        } else {
            return '';
        }
    }
}

function imageFormatter(cellvalue, options, rowdata) {
    if (cellvalue != null) {
        return ' <img src="' + cellvalue + '" align="middle" style="width:36px;height:36px;" />';
    }
    return '';
}

function forbiddenFormatter(cellvalue, options, rowdata) {
    if (cellvalue = 1) {
        return '<span class="label label-success">正常</span>';
    } else if (cellvalue = 0) {
        return '<span class="label label-danger">禁用</span>';
    }
    return '';
}

function phoneverifyFormatter(cellvalue, options, rowdata) {
    if (cellvalue = 1) {
        return '是';
    } else if (cellvalue = 0) {
        return '否';
    }
    return '';
}

var vm = new Vue({
    el: '#rrapp',
    data: {
        q: {
            username: null
        },
        m: {
            money: null,
            remark: null
        },
        showDiag: 0, // 0:列表、1:编辑页、2:充值页、3:提现页
        title: null,
        user: {},
        param: {}
    },
    methods: {
        query: function () {
            vm.reload();
        },
        add: function () {
            vm.showDiag = 1;
            vm.title = "新增";
            vm.user = {};
        },

        update: function (event) {
            var id = getSelectedRow();
            if (id == null) {
                return;
            }
            vm.showDiag = 1;
            vm.title = "修改";

            vm.getInfo(id)
        },
        saveOrUpdate: function (event) {
            $('#btnSaveOrUpdate').button('loading').delay(1000).queue(function () {
                var url = vm.user.id == null ? "manage/user/save" : "manage/user/update";
                $.ajax({
                    type: "POST",
                    url: baseURL + url,
                    contentType: "application/json",
                    data: JSON.stringify(vm.user),
                    success: function (r) {
                        if (r.code === 0) {
                            layer.msg("操作成功", {icon: 1});
                            vm.reload();
                        } else {
                            layer.alert(r.msg);
                        }
                        $('#btnSaveOrUpdate').button('reset');
                        $('#btnSaveOrUpdate').dequeue();
                    }
                });
            });
        },

        del: function (event) {
            var ids = getSelectedRows();
            if (ids == null) {
                return;
            }
            var lock = false;
            layer.confirm('确定要删除选中的记录？', {
                btn: ['确定', '取消'] //按钮
            }, function () {
                if (!lock) {
                    lock = true;
                    $.ajax({
                        type: "POST",
                        url: baseURL + "manage/user/delete",
                        contentType: "application/json",
                        data: JSON.stringify(ids),
                        success: function (r) {
                            if (r.code == 0) {
                                layer.msg("操作成功", {icon: 1});
                                $("#jqGrid").trigger("reloadGrid");
                            } else {
                                layer.alert(r.msg);
                            }
                        }
                    });
                }
            }, function () {
            });
        },
        getInfo: function (id) {
            $.get(baseURL + "manage/user/info/" + id, function (r) {
                vm.user = r.user;
            });
        },
        // 充值
        topup: function (event) {
            var id = getSelectedRow();
            if (id == null) {
                return;
            }
            vm.showDiag = 2;
            vm.title = "充值";

            vm.getInfo(id)
        },
        topupUpdate: function (event) {
            vm.param = {
                'userid': vm.user.id,
                'username': vm.user.accountName,
                'nickname': vm.user.name,
                'type': 1,
                'money': vm.m.money,
                'remark': vm.m.remark,
                'operater': $(parent.document.getElementById("userId")).val(),
                'operatername': $(parent.document.getElementById("username")).val()
            };
            $('#btnTopup').button('loading').delay(1000).queue(function () {
                $.ajax({
                    type: "POST",
                    url: baseURL + "manage/user/topup",
                    contentType: "application/json",
                    data: JSON.stringify(vm.param),
                    success: function (r) {
                        if (r.code === 0) {
                            layer.msg("操作成功", {icon: 1});
                            vm.reload();
                        } else {
                            layer.alert(r.msg);
                        }
                        $('#btnTopup').button('reset');
                        $('#btnTopup').dequeue();
                    }
                });
            });
        },
        // 提现
        cash: function (event) {
            var id = getSelectedRow();
            if (id == null) {
                return;
            }
            vm.showDiag = 3;
            vm.title = "提现";

            vm.getInfo(id)
        },
        cashUpdate: function (event) {
            $('#btnCash').button('loading').delay(1000).queue(function () {
                vm.param = {
                    'userid': vm.user.id,
                    'username': vm.user.accountName,
                    'nickname': vm.user.name,
                    'type': 2,
                    'money': vm.m.money,
                    'remark': vm.m.remark,
                    'operater': $(parent.document.getElementById("userId")).val(),
                    'operatername': $(parent.document.getElementById("username")).val()
                };
                $.ajax({
                    type: "POST",
                    url: baseURL + "manage/user/cash",
                    contentType: "application/json",
                    data: JSON.stringify(vm.param),
                    success: function (r) {
                        if (r.code === 0) {
                            layer.msg("操作成功", {icon: 1});
                            vm.reload();
                        } else {
                            layer.alert(r.msg);
                        }
                        $('#btnCash').button('reset');
                        $('#btnCash').dequeue();
                    }
                });
            });
        },
        reload: function (event) {
            vm.showDiag = 0;
            var page = $("#jqGrid").jqGrid('getGridParam', 'page');
            $("#jqGrid").jqGrid('setGridParam', {
                postData: {'username': vm.q.username},
                page: page
            }).trigger("reloadGrid");
        }
    }
});
