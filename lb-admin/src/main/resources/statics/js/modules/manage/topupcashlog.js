$(function () {
    $("#jqGrid").jqGrid({
        url: baseURL + 'manage/topupcashlog/list',
        datatype: "json",
        colModel: [
			{ label: 'id', name: 'id', index: 'id', width: 50, key: true },
			{ label: '用户id', name: 'userid', index: 'userid', width: 80 },
			{ label: '用户名', name: 'username', index: 'username', width: 80 },
			{ label: '用户昵称', name: 'nickname', index: 'nickname', width: 80 },
			{ label: '操作类型', name: 'type', index: 'type', width: 80, formatter: typeFormatter },
			{ label: '金额', name: 'money', index: 'money', width: 80 },
			{ label: '备注', name: 'remark', index: 'remark', width: 80 },
			{ label: '操作人id', name: 'operater', index: 'operater', width: 80 },
			{ label: '操作人姓名', name: 'operatername', index: 'operatername', width: 80 },
			{ label: '操作时间', name: 'operatetime', index: 'operatetime', width: 80 }
        ],
		viewrecords: true,
        height: 385,
        rowNum: 10,
		rowList : [10,30,50],
        rownumbers: true,
        rownumWidth: 25,
        autowidth:true,
        multiselect: true,
        pager: "#jqGridPager",
        jsonReader : {
            root: "page.list",
            page: "page.currPage",
            total: "page.totalPage",
            records: "page.totalCount"
        },
        prmNames : {
            page:"page",
            rows:"limit",
            order: "order"
        },
        gridComplete:function(){
        	//隐藏grid底部滚动条
        	$("#jqGrid").closest(".ui-jqgrid-bdiv").css({ "overflow-x" : "hidden" });
        }
    });
});

function typeFormatter(cellvalue, options, rowdata) {
    if (cellvalue != null) {
        if (cellvalue == 1) {
            return '充值';
        } else if (cellvalue == 2) {
            return '提现';
        } else {
            return '';
        }
    }
}

var vm = new Vue({
	el:'#rrapp',
	data:{
        q: {
            username: null
        },
		showList: true,
		title: null,
		topupcashLog: {}
	},
	methods: {
		query: function () {
			vm.reload();
		},
		add: function(){
			vm.showList = false;
			vm.title = "新增";
			vm.topupcashLog = {};
		},
		update: function (event) {
			var id = getSelectedRow();
			if(id == null){
				return ;
			}
			vm.showList = false;
            vm.title = "修改";

            vm.getInfo(id)
		},
		saveOrUpdate: function (event) {
		    $('#btnSaveOrUpdate').button('loading').delay(1000).queue(function() {
                var url = vm.topupcashLog.id == null ? "manage/topupcashlog/save" : "manage/topupcashlog/update";
                $.ajax({
                    type: "POST",
                    url: baseURL + url,
                    contentType: "application/json",
                    data: JSON.stringify(vm.topupcashLog),
                    success: function(r){
                        if(r.code === 0){
                             layer.msg("操作成功", {icon: 1});
                             vm.reload();
                             $('#btnSaveOrUpdate').button('reset');
                             $('#btnSaveOrUpdate').dequeue();
                        }else{
                            layer.alert(r.msg);
                            $('#btnSaveOrUpdate').button('reset');
                            $('#btnSaveOrUpdate').dequeue();
                        }
                    }
                });
			});
		},
		del: function (event) {
			var ids = getSelectedRows();
			if(ids == null){
				return ;
			}
			var lock = false;
            layer.confirm('确定要删除选中的记录？', {
                btn: ['确定','取消'] //按钮
            }, function(){
               if(!lock) {
                    lock = true;
		            $.ajax({
                        type: "POST",
                        url: baseURL + "manage/topupcashlog/delete",
                        contentType: "application/json",
                        data: JSON.stringify(ids),
                        success: function(r){
                            if(r.code == 0){
                                layer.msg("操作成功", {icon: 1});
                                $("#jqGrid").trigger("reloadGrid");
                            }else{
                                layer.alert(r.msg);
                            }
                        }
				    });
			    }
             }, function(){
             });
		},
		getInfo: function(id){
			$.get(baseURL + "manage/topupcashlog/info/"+id, function(r){
                vm.topupcashLog = r.topupcashLog;
            });
		},
		reload: function (event) {
			vm.showList = true;
			var page = $("#jqGrid").jqGrid('getGridParam','page');
			$("#jqGrid").jqGrid('setGridParam',{
                postData: {'username': vm.q.username},
                page:page
            }).trigger("reloadGrid");
		}
	}
});
