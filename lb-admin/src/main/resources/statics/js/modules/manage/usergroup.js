$(function () {
    $("#jqGrid").jqGrid({
        url: baseURL + 'manage/usergroup/list',
        datatype: "json",
        colModel: [
            { label: '编号', name: 'id', width: 20 },
			{ label: '用户', name: 'userName', width: 80 },
			{ label: '群组', name: 'groupName', width: 80 },
			{ label: '在群昵称', name: 'nick', width: 80 },
			{ label: '在群角色', name: 'roleName', width: 80 }
        ],
		viewrecords: true,
        height: 385,
        rowNum: 10,
		rowList : [10,30,50],
        rownumbers: true,
        rownumWidth: 25,
        autowidth:true,
        multiselect: true,
        pager: "#jqGridPager",
        jsonReader : {
            root: "page.list",
            page: "page.currPage",
            total: "page.totalPage",
            records: "page.totalCount"
        },
        prmNames : {
            page:"page",
            rows:"limit",
            order: "order"
        },
        gridComplete:function(){
        	//隐藏grid底部滚动条
        	$("#jqGrid").closest(".ui-jqgrid-bdiv").css({ "overflow-x" : "hidden" });
        }
    });
});

var vm = new Vue({
	el:'#rrapp',
	data:{
        q: {
            param: null
        },
		showList: true,
		title: null,
		userGroup: {},
		roleList: {}
	},
    mounted: function mounted(){
        //加载群组角色字典列表
        $.get(baseURL + "sys/dict/typelist/grouprole", function(r){
            vm.roleList = r.list;
        })
    },
	methods: {
		query: function () {
			vm.reload();
		},
		add: function(){
			vm.showList = false;
			vm.title = "新增";
			vm.userGroup = {};
		},
		update: function (event) {
			var id = getSelectedRow();
			if(id == null){
				return ;
			}
			vm.showList = false;
            vm.title = "修改";

            vm.getInfo(id)
		},
        saveOrUpdate: function (event) {
		    $('#btnSaveOrUpdate').button('loading').delay(1000).queue(function() {
		        console.log('vm.userGroup.role:'+vm.userGroup.role);
                var url = vm.userGroup.id == null ? "manage/usergroup/save" : "manage/usergroup/update";
                $.ajax({
                    type: "POST",
                    url: baseURL + url,
                    contentType: "application/json",
                    data: JSON.stringify(vm.userGroup),
                    success: function(r){
                        if(r.code === 0){
                             layer.msg("操作成功", {icon: 1});
                             vm.reload();
                             $('#btnSaveOrUpdate').button('reset');
                             $('#btnSaveOrUpdate').dequeue();
                        }else{
                            layer.alert(r.msg);
                            $('#btnSaveOrUpdate').button('reset');
                            $('#btnSaveOrUpdate').dequeue();
                        }
                    }
                });
			});
		},
        roleRadio: function(){
            layer.open({
                type: 1,
                offset: '50px',
                skin: 'layui-layer-molv',
                title: "选择角色",
                area: ['200px', '300px'],
                shade: 0,
                shadeClose: false,
                content: jQuery("#dictLayer"),
                btn: ['确定', '取消'],
                btn1: function (index) {
                    //选择角色
                    // vm.user.deptId = node[0].deptId;
                    // vm.user.deptName = node[0].name;

                    layer.close(index);
                }
            });
        },
		del: function (event) {
			var ids = getSelectedRows();
			if(ids == null){
				return ;
			}
			var lock = false;
            layer.confirm('确定要删除选中的记录？', {
                btn: ['确定','取消'] //按钮
            }, function(){
               if(!lock) {
                    lock = true;
		            $.ajax({
                        type: "POST",
                        url: baseURL + "manage/usergroup/delete",
                        contentType: "application/json",
                        data: JSON.stringify(ids),
                        success: function(r){
                            if(r.code == 0){
                                layer.msg("操作成功", {icon: 1});
                                $("#jqGrid").trigger("reloadGrid");
                            }else{
                                layer.alert(r.msg);
                            }
                        }
				    });
			    }
             }, function(){
             });
		},
		getInfo: function(id){
			$.get(baseURL + "manage/usergroup/info/"+id, function(r){
                vm.userGroup = r.userGroup;
            });
		},
		reload: function (event) {
			vm.showList = true;
			var page = $("#jqGrid").jqGrid('getGridParam','page');
			$("#jqGrid").jqGrid('setGridParam',{
                postData: {'param': vm.q.param},
                page:page
            }).trigger("reloadGrid");
		}
	}
});
